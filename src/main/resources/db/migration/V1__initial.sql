CREATE SCHEMA friendship;

CREATE TABLE friendship.friend
(
    player1       INTEGER                     NOT NULL,
    player2       INTEGER                     NOT NULL,
    create_time   timestamp without time zone NOT NULL,
    social_type   SMALLINT                    NOT NULL,
    friend_status SMALLINT                    NOT NULL,
    CONSTRAINT friend_id PRIMARY KEY (player1, player2),
    CONSTRAINT friend_player1_less_than_player2 CHECK (player1 < player2)
);

CREATE INDEX friend_player2_id_idx ON friendship.friend USING btree (player2);

COMMENT
    ON TABLE friendship.friend IS 'Friends';
COMMENT
    ON COLUMN friendship.friend.player1 IS 'First player';
COMMENT
    ON COLUMN friendship.friend.player2 IS 'Second player';
COMMENT
    ON COLUMN friendship.friend.social_type IS 'Social type';
COMMENT
    ON COLUMN friendship.friend.create_time IS 'Create time';
COMMENT
    ON COLUMN friendship.friend.friend_status IS 'Friend status';