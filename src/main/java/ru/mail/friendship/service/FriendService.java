package ru.mail.friendship.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.friendship.dto.DTOMapper;
import ru.mail.friendship.dto.FriendDTO;
import ru.mail.friendship.dto.PageData;
import ru.mail.friendship.entity.FriendEntity;
import ru.mail.friendship.repositories.FriendRepository;

import java.time.Duration;
import java.time.LocalDateTime;

import static ru.mail.friendship.entity.FriendStatus.*;

@SuppressWarnings("FinalOrAbstractClass")
@Service
public class FriendService {
  private static final long MAX_INVITES = 3;
  private static final @NotNull Duration EXPIRED_PERIOD = Duration.ofSeconds(20);

  @Autowired
  private @NotNull FriendRepository friendRepository;
  @Autowired
  private @NotNull DTOMapper mapper;

  private static @NotNull Specification<FriendEntity> playerInvitesSpec(int playerId) {
    return (root, query, builder) -> builder.or(
        builder.and(
            builder.equal(root.get("player1"), playerId),
            builder.equal(root.get("friendStatus"), Inviter)
        ),
        builder.and(
            builder.equal(root.get("player2"), playerId)),
        builder.equal(root.get("friendStatus"), Invitee)
    );
  }

  private static @NotNull Specification<FriendEntity> expiredInvitesSpec() {
    return (root, query, cb) -> cb.and(
        cb.lessThan(root.get("createTime"), LocalDateTime.now().minus(EXPIRED_PERIOD)),
        cb.notEqual(root.get("friendStatus"), Friend)
    );
  }

  private static @NotNull Specification<FriendEntity> friendsSpec(int player1) {
    return (root, query, cb) -> cb.and(
        cb.or(cb.equal(root.get("player1"), player1), cb.equal(root.get("player2"), player1)),
        cb.equal(root.get("friendStatus"), Friend)
    );
  }

  @Transactional(readOnly = true)
  public @NotNull PageData<FriendDTO> getFriends(int playerId, int page, int pageSize) {
    final var pageData = friendRepository.findAll(friendsSpec(playerId), PageRequest.of(page, pageSize));

    return new PageData<>(pageData.getTotalElements(), mapper.friendDTO(pageData.getContent()));
  }

  @Transactional
  public @NotNull FriendDTO addFriend(int playerId, int friendId) {
    final var friendship = friendRepository.getFriendship(Math.min(playerId, friendId), Math.max(playerId, friendId));
    if (friendship == null) {
      throw new IllegalStateException("Invitation wasn't found!");
    }

    friendship.setFriendStatus(Friend);
    return mapper.friendDTO(friendRepository.save(friendship));
  }

  @Transactional
  public boolean deleteFriendship(int player1, int player2) {
    final var friendship = friendRepository.getFriendship(Math.min(player1, player2), Math.max(player1, player2));
    if (friendship == null || friendship.getFriendStatus() != Friend) {
      return false;
    }

    friendRepository.delete(friendship);
    return true;
  }
}
