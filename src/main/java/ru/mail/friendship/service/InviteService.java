package ru.mail.friendship.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.friendship.dto.DTOMapper;
import ru.mail.friendship.dto.FriendDTO;
import ru.mail.friendship.dto.PageData;
import ru.mail.friendship.entity.FriendEntity;
import ru.mail.friendship.entity.FriendStatus;
import ru.mail.friendship.repositories.FriendRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static ru.mail.friendship.entity.FriendStatus.*;

@SuppressWarnings("FinalOrAbstractClass")
@Service
public class InviteService {
  private static final long MAX_INVITES = 3;
  private static final @NotNull Duration EXPIRED_PERIOD = Duration.ofSeconds(20);

  @Autowired
  private @NotNull FriendRepository friendRepository;
  @Autowired
  private @NotNull DTOMapper mapper;

  private static @NotNull Specification<FriendEntity> invitesSpec(int player1) {
    return (root, query, cb) -> cb.and(
        cb.or(cb.equal(root.get("player1"), player1), cb.equal(root.get("player2"), player1)),
        cb.or(cb.equal(root.get("friendStatus"), Invitee), cb.equal(root.get("friendStatus"), Inviter))
    );
  }

  private static @NotNull Specification<FriendEntity> playerInvitesSpec(int playerId) {
    return (root, query, builder) -> builder.or(
        builder.and(
            builder.equal(root.get("player1"), playerId),
            builder.equal(root.get("friendStatus"), Inviter)
        ),
        builder.and(
            builder.equal(root.get("player2"), playerId)),
        builder.equal(root.get("friendStatus"), Invitee)
    );
  }

  private static @NotNull Specification<FriendEntity> expiredInvitesSpec() {
    return (root, query, cb) -> cb.and(
        cb.lessThan(root.get("createTime"), LocalDateTime.now().minus(EXPIRED_PERIOD)),
        cb.notEqual(root.get("friendStatus"), Friend)
    );
  }

  @Transactional(readOnly = true)
  public @NotNull PageData<FriendDTO> getInvites(int playerId, int page, int pageSize) {
    final var pageData = friendRepository.findAll(invitesSpec(playerId), PageRequest.of(page, pageSize));

    return new PageData<>(pageData.getTotalElements(), mapper.friendDTO(pageData.getContent()));
  }

  @Transactional
  public @NotNull FriendDTO createInvite(int playerId, int friendId, short socialType) {
    final var invitesCount = friendRepository.count(playerInvitesSpec(playerId));
    if (invitesCount >= MAX_INVITES) {
      throw new IllegalStateException("Too much invites");
    }

    final var friendEntity = new FriendEntity();
    friendEntity.setPlayer1(Math.min(playerId, friendId));
    friendEntity.setPlayer2(Math.max(playerId, friendId));
    friendEntity.setFriendStatus(playerId < friendId ? Inviter : Invitee);
    friendEntity.setSocialType(socialType);

    return mapper.friendDTO(friendRepository.save(friendEntity));
  }

  @Transactional
  public boolean deleteInvite(int player1, int player2) {
    final var friendship = friendRepository.getFriendship(Math.min(player1, player2), Math.max(player1, player2));
    if (friendship == null || friendship.getFriendStatus() != Invitee && friendship.getFriendStatus() != Inviter) {
      return false;
    }

    friendRepository.delete(friendship);
    return true;
  }

  @Transactional
  public int deleteExpiredInvites() {
    final var expired = friendRepository.findAll(expiredInvitesSpec());
    friendRepository.deleteAll(expired);

    return expired.size();
  }
}
