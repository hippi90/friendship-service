package ru.mail.friendship.controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mail.friendship.dto.FriendDTO;
import ru.mail.friendship.dto.PageData;
import ru.mail.friendship.dto.PageableRequest;
import ru.mail.friendship.service.InviteService;

@SuppressWarnings("FinalOrAbstractClass")
@RestController
@RequestMapping("/invite")
public class InviteController {

  @Autowired
  private @NotNull InviteService inviteService;

  @GetMapping("{playerId}")
  public @NotNull PageData<FriendDTO> getInvites(@PathVariable("playerId") int playerId, @NotNull PageableRequest pageableRequest) {
    return inviteService.getInvites(playerId, pageableRequest.getPage(), pageableRequest.getPageSize());
  }

  @PostMapping("{playerId}")
  public @NotNull FriendDTO createInvite(@PathVariable("playerId") int playerId,
                                         @RequestParam("friendId") int friendId,
                                         @RequestParam("socialType") short socialType) {
    return inviteService.createInvite(playerId, friendId, socialType);
  }

  @DeleteMapping("{playerId}")
  public boolean deleteInvite(@PathVariable("playerId") int playerId,
                              @RequestParam("friendId") int friendId) {
    return inviteService.deleteInvite(playerId, friendId);
  }
}
