package ru.mail.friendship.controllers;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mail.friendship.dto.FriendDTO;
import ru.mail.friendship.dto.PageData;
import ru.mail.friendship.dto.PageableRequest;
import ru.mail.friendship.service.FriendService;

@SuppressWarnings("FinalOrAbstractClass")
@RestController
@RequestMapping("/friend")
public class FriendController {

  @Autowired
  private @NotNull FriendService friendService;

  @GetMapping("{playerId}")
  public @NotNull PageData<FriendDTO> getFriends(@PathVariable("playerId") int playerId, @NotNull PageableRequest pageableRequest) {
    return friendService.getFriends(playerId, pageableRequest.getPage(), pageableRequest.getPageSize());
  }

  @PostMapping("{playerId}")
  public @NotNull FriendDTO addFriend(@PathVariable("playerId") int playerId,
                                      @RequestParam("friendId") int friendId) {
    return friendService.addFriend(playerId, friendId);
  }

  @DeleteMapping("{playerId}")
  public boolean deleteFriendship(@PathVariable("playerId") int playerId,
                                  @RequestParam("friendId") int friendId) {
    return friendService.deleteFriendship(playerId, friendId);
  }
}
