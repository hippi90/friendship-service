package ru.mail.friendship.entity;

import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("FinalOrAbstractClass")
public class FriendEntityPK implements Serializable {
  private int player1;
  private int player2;

  @Column(name = "player1")
  @Id
  public int getPlayer1() {
    return player1;
  }

  public void setPlayer1(int player1) {
    this.player1 = player1;
  }

  @Column(name = "player2")
  @Id
  public int getPlayer2() {
    return player2;
  }

  public void setPlayer2(int player2) {
    this.player2 = player2;
  }

  @Override
  public boolean equals(@Nullable Object obj) {
    if (this == obj) return true;
    if (obj == null || !getClass().equals(obj.getClass())) return false;
    final var another = (FriendEntityPK) obj;
    if (player1 != another.player1) return false;
    return player2 == another.player2;
  }

  @Override
  public int hashCode() {
    return Objects.hash(player1, player2);
  }
}
