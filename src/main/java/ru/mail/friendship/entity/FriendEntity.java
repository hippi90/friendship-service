package ru.mail.friendship.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@SuppressWarnings("FinalOrAbstractClass")
@Entity
@Table(name = "friend", schema = "friendship", catalog = "friendship_demo")
@IdClass(FriendEntityPK.class)
public class FriendEntity {
  private int player1;
  private int player2;
  @Temporal(TemporalType.TIMESTAMP)
  private @NotNull LocalDateTime createTime = LocalDateTime.now();
  private short socialType;

  @Enumerated(EnumType.ORDINAL)
  private @NotNull FriendStatus friendStatus;

  @Id
  @Column(name = "player1")
  public int getPlayer1() {
    return player1;
  }

  public void setPlayer1(int player1) {
    this.player1 = player1;
  }

  @Id
  @Column(name = "player2")
  public int getPlayer2() {
    return player2;
  }

  public void setPlayer2(int player2) {
    this.player2 = player2;
  }

  @Basic
  @Column(name = "create_time")
  public @NotNull LocalDateTime getCreateTime() {
    return createTime;
  }

  public void setCreateTime(@NotNull LocalDateTime createTime) {
    this.createTime = createTime;
  }

  @Basic
  @Column(name = "social_type")
  public short getSocialType() {
    return socialType;
  }

  public void setSocialType(short socialType) {
    this.socialType = socialType;
  }

  @Basic
  @Column(name = "friend_status")
  public @NotNull FriendStatus getFriendStatus() {
    return friendStatus;
  }

  public void setFriendStatus(@NotNull FriendStatus friendStatus) {
    this.friendStatus = friendStatus;
  }

  @Override
  public boolean equals(@Nullable Object obj) {
    if (this == obj) return true;
    if (obj == null || !getClass().equals(obj.getClass())) return false;
    final var another = (FriendEntity) obj;
    if (player1 != another.player1) return false;
    if (player2 != another.player2) return false;
    if (socialType != another.socialType) return false;
    if (friendStatus != another.friendStatus) return false;
    return Objects.equals(createTime, another.createTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(player1, player2, createTime, socialType, friendStatus);
  }
}
