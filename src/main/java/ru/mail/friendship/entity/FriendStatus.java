package ru.mail.friendship.entity;

public enum FriendStatus {
  None,
  Inviter,
  Invitee,
  Friend,
  ;
}
