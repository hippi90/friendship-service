package ru.mail.friendship;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SuppressWarnings("FinalOrAbstractClass")
@SpringBootApplication
@EnableScheduling
public class FriendshipServiceApplication {

	public static void main(@NotNull String[] args) {
		SpringApplication.run(FriendshipServiceApplication.class, args);
	}

}
