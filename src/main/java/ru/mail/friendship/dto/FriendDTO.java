package ru.mail.friendship.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.friendship.entity.FriendStatus;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
public final class FriendDTO {
  private final int player1;
  private final int player2;
  private final long createTime;
  private final short socialType;
  private @NotNull FriendStatus friendStatus;

  public FriendDTO(int player1, int player2, long createTime, short socialType, @NotNull FriendStatus friendStatus) {
    this.player1 = player1;
    this.player2 = player2;
    this.createTime = createTime;
    this.socialType = socialType;
    this.friendStatus = friendStatus;
  }
}
