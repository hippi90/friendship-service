package ru.mail.friendship.dto;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

@Data
public final class PageData<T> {
  private final long count;
  private final @NotNull Collection<T> data;

  public PageData(long count, @NotNull Collection<T> data) {
    this.count = count;
    this.data = data;
  }
}
