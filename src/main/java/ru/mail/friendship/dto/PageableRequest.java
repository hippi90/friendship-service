package ru.mail.friendship.dto;

import lombok.Data;

@Data
public final class PageableRequest {
  private int page;
  private int pageSize = 10;
}
