package ru.mail.friendship.dto;

import org.jetbrains.annotations.NotNull;
import org.mapstruct.Mapper;
import ru.mail.friendship.entity.FriendEntity;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Mapper(componentModel = "spring")
public interface DTOMapper {
  @NotNull FriendDTO friendDTO(@NotNull FriendEntity entity);

  @NotNull List<FriendDTO> friendDTO(@NotNull List<FriendEntity> entity);

  default long localDateTime(@NotNull LocalDateTime time) {
    return time.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
  }
}
