package ru.mail.friendship.schedule;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.mail.friendship.service.InviteService;

@SuppressWarnings("FinalOrAbstractClass")
@Component
public class InviteSchedule {
private static final @NotNull Logger LOG = LoggerFactory.getLogger(InviteSchedule.class);

  @Autowired
  private @NotNull InviteService inviteService;

  @Scheduled(cron = "0/10 * * * * *")
  public void deleteExpiredInvites() {
    LOG.info("Start scheduled task...");

    final var removedCount = inviteService.deleteExpiredInvites();
    LOG.info("Removed {} entities", removedCount);
  }
}
