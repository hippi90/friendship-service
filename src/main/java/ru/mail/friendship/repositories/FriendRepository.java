package ru.mail.friendship.repositories;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.mail.friendship.entity.FriendEntity;
import ru.mail.friendship.entity.FriendEntityPK;

public interface FriendRepository extends JpaRepository<FriendEntity, FriendEntityPK>, JpaSpecificationExecutor<FriendEntity> {
  @Query("select f from FriendEntity f where f.player1 = :player1 and f.player2 = :player2")
  @Nullable FriendEntity getFriendship(@Param("player1") int player1, @Param("player2") int player2);
}
